terraform {
  backend "http" {
  }

  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.7.0"
    }
    civo = {
      source  = "civo/civo"
      version = "0.10.3"
    }
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}
