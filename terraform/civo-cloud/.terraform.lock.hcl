# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/civo/civo" {
  version     = "0.10.3"
  constraints = "0.10.3"
  hashes = [
    "h1:60lJIvFEU7cFe4jDL4zalq+bnD8/Kk8GRZD7x7abK4E=",
    "zh:182089ac11a0c31055d11306b686a6fe06a8f8d31c3024751c1957bf2a3b4eba",
    "zh:1eb8cb8c77e893bb31ac8806995fe40c088a517c27ea3a45c3bef0dbd11e28ae",
    "zh:25b16de5219d7aaa55eda6a37a2ddcd9599a5285b0b0a611bd15a1c228538059",
    "zh:2b0d95eae098aba00e4f4d2fc11ae34518df2337dc56d621fbdb60aa293209f9",
    "zh:580bfd320609087c62e6e79e470e1b117a7f710d0a64c6895373236cc2f50b53",
    "zh:6860453fdd14a17ec20a5f578c514aecbca454e089459329faaad067d4fff98d",
    "zh:885000117981d5e623c56131a5a9743ea091ae93dd600a82b8bc230edcb325ab",
    "zh:bbc0aa53cc17d8894b583bb54b516832abbb7c2487e563c5353360ab044c603c",
    "zh:bdbbfa2df9b13e7c8a257db687ed5532be0606257762b2afa7c4788f6a255f43",
    "zh:c5b9e4738cb72484faf4a5d352751a91b23a2c9a499a3aa229d331af2a21c205",
    "zh:ccf755dffb2d1e101491178b64401f93e4922e967060552c6d51af101ae1a96d",
    "zh:e0d0f5e73bb5bb7ef8725dd92c718315ff27379a84e80b0fe215aad5097ac97e",
  ]
}

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.7.0"
  constraints = "3.7.0"
  hashes = [
    "h1:89Pa83NutpdjTeiZbBk3hCM7iFAqtMBWM+3v1Wfm6Y0=",
    "zh:16addba6bda82e6689d21049392d296ce276cb93cbc5bcf3ad21f7d39cd820fd",
    "zh:1e9dd3db81a38d3112ddb24cec5461d919e7770a431f46ac390469e448716fd4",
    "zh:252c08d473d938c2da2711838db2166ecda2a117f64a10d279e935546ef7d991",
    "zh:2e0c83da0ba44e0521cb477dd60ea7c08b9edbc44b607e5ddb963becb26970a5",
    "zh:396223391782f1f809a60f045bfdcde41820d0d6119912718d86fc153fc28969",
    "zh:3a6b3c0901b81bc451d1ead2a2f26845d5db6b6253758c1f0aa0bad53fb6b4bd",
    "zh:51010e8f1d05f4979f0e10cf0e3b56cec13c731d99f373dda9fd9561ddb2826b",
    "zh:53ef55edf7698cbb4562265a6ab9e261716f62a82590e5fb6ad4f7f7458bdc5c",
    "zh:6c2db10e6226cc748e6dd5c1cbc257fda30cd58a175a32fc95a8ccd5cebdd3e7",
    "zh:91627f5af7e8315479a6c45cb1ae5db3c0a91a18018383cd409f3cfa04408aed",
    "zh:b5217a81cfc58334278831eacb2865bd8fc025b0cb1c576e9da9c4dc3a187ef5",
    "zh:c70afea4324518b099d23abc189dff22e6706ca0936de39eca01851e2550af7e",
    "zh:e62c212169ef9aad3b01f721db03b7e08d7d4acbbac67a713f06239a3a931834",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.1.0"
  constraints = "3.1.0"
  hashes = [
    "h1:rKYu5ZUbXwrLG1w81k7H3nce/Ys6yAxXhWcbtk36HjY=",
    "zh:2bbb3339f0643b5daa07480ef4397bd23a79963cc364cdfbb4e86354cb7725bc",
    "zh:3cd456047805bf639fbf2c761b1848880ea703a054f76db51852008b11008626",
    "zh:4f251b0eda5bb5e3dc26ea4400dba200018213654b69b4a5f96abee815b4f5ff",
    "zh:7011332745ea061e517fe1319bd6c75054a314155cb2c1199a5b01fe1889a7e2",
    "zh:738ed82858317ccc246691c8b85995bc125ac3b4143043219bd0437adc56c992",
    "zh:7dbe52fac7bb21227acd7529b487511c91f4107db9cc4414f50d04ffc3cab427",
    "zh:a3a9251fb15f93e4cfc1789800fc2d7414bbc18944ad4c5c98f466e6477c42bc",
    "zh:a543ec1a3a8c20635cf374110bd2f87c07374cf2c50617eee2c669b3ceeeaa9f",
    "zh:d9ab41d556a48bd7059f0810cf020500635bfc696c9fc3adab5ea8915c1d886b",
    "zh:d9e13427a7d011dbd654e591b0337e6074eef8c3b9bb11b2e39eaaf257044fd7",
    "zh:f7605bd1437752114baf601bdf6931debe6dc6bfe3006eb7e9bb9080931dca8a",
  ]
}
