terraform {
  backend "http" {
  }

  required_providers {
    civo = {
      source  = "civo/civo"
      version = "0.10.3"
    }
    random = {
      source = "hashicorp/random"
      version = "3.1.0"
    }
  }
}
